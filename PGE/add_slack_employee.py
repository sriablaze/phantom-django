from PGE.models import Employee
from PGE.models import User


def add_slack_employee(id, username, email):
	user, created = User.objects.get_or_create(username=username, email=email)
	emp = Employee(user=user, unique_id=id)
	emp.save()
