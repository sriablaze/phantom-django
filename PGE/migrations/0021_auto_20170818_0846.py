# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-08-18 03:16
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('PGE', '0020_auto_20170817_1801'),
    ]

    operations = [
        migrations.CreateModel(
            name='Discussion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('from_date_time', models.DateTimeField(default=None, null=True)),
                ('to_date_time', models.DateTimeField(default=None, null=True)),
            ],
        ),
        migrations.AlterField(
            model_name='project',
            name='start_date',
            field=models.DateField(default=datetime.date(2017, 8, 18)),
        ),
        migrations.AlterField(
            model_name='session',
            name='start_date_time',
            field=models.DateTimeField(default=datetime.datetime(2017, 8, 18, 8, 45, 58, 846382)),
        ),
        migrations.AlterField(
            model_name='task',
            name='start_date',
            field=models.DateField(default=datetime.date(2017, 8, 18)),
        ),
        migrations.AddField(
            model_name='discussion',
            name='task',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='PGE.Task'),
        ),
        migrations.AddField(
            model_name='discussion',
            name='users',
            field=models.ManyToManyField(to='PGE.Employee'),
        ),
    ]
