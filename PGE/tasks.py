from celery.decorators import task

import json
import requests

SLACK_MULTICAST_URL = "http://139.59.24.25:8000"

@task(name="notify employees")
def notify_employees(message, channel_list, team_id):
        url = SLACK_MULTICAST_URL + "/notify/channels"
        print(message)
        print(channel_list)
        request = {
                'message' : message,
                'channel_list' : channel_list,
                'team_id' : team_id
        }
        request_json = json.dumps(request)
        print(request_json)
        headers = {'Content-Type': 'application/json'}
        multicast_request = requests.post(url, data=request_json, headers=headers)
        return



@task(name="notify new team")
def notify_new_team(team_id, access_token):

        url = SLACK_MULTICAST_URL + "/notify/new/team"

        request = {
                'team_id': team_id,
                'access_token': access_token
        }
        request_json = json.dumps(request)
        print(request_json)
        headers = {'Content-Type': 'application/json'}
        multicast_request = requests.post(url, data=request_json, headers=headers)
        return

