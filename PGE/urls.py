from django.conf.urls import include
from django.conf.urls import url

from PGE.views import add_employee, assign_tasks_gui, add_tasks, render_home, create_project, count_disjuncts_of_user, developer_stage_instantiation, get_channels, get_access_tokens, get_all_employees, get_employees, get_project_employees, get_projects_of_employee, handle_manager_view, handle_oauth_flow, list_task_employees, handle_message, handle_queries, list_all_projects, list_todo_tasks, list_doing_tasks, list_done_tasks, list_todo_disjuncts, list_doing_disjuncts, list_done_disjuncts, list_managers, list_links, submit_link_data, handle_slack_interaction, handle_github_hook

urlpatterns = [
	#url(r'^', render_home, name='render_home'),
	url(r'^developer', developer_stage_instantiation, name='developer_stage_instantiation'),
	url(r'^list/access/tokens', get_access_tokens, name='get_access_tokens'),
	url(r'^(?P<team_id>[A-Z0-9]{0,9})/web/view/manager/(?P<channel_id>[0-9]{0,2})', handle_manager_view, name='handle_manager_view'),
	url(r'^web/query', handle_queries, name='handle_queries'),
	url(r'^(?P<team_id>[A-Z0-9]{0,9})/web/create/project', create_project, name='create_project'),
	url(r'^(?P<team_id>[A-Z0-9]{0,9})/web/assign/task', assign_tasks_gui, name='assign_tasks_gui'),
	url(r'^github/hook', handle_github_hook, name='handle_github_hook'),
	url(r'^slack/interaction', handle_slack_interaction, name='handle_slack_interaction'),
	url(r'^slack/oauth', handle_oauth_flow, name='handle_oauth_flow'),
	url(r'^submitTasks', add_tasks, name = "add_tasks"),
	url(r'^submitMessage', handle_message, name="handle_message"),
	url(r'^submitLinkData', submit_link_data, name="submit_link_data"),
	url(r'^addEmployee', add_employee, name="add_employee"),
	url(r'^getEmployees/(?P<role>[A-Z]+)', get_employees, name="get_employees"),
	url(r'^getChannels/(?P<email>[^@]+@[^@]+\.[^@]+)', get_channels, name="get_channels"),
	url(r'^list/projects/employee/(?P<employee_id>[0-9]{0,2})', get_projects_of_employee, name="get_projects_of_employee"),
	url(r'^(?P<team_id>[A_Z0-9]{0,9})/list/employees/(?P<channel_id>[0-9]{0,2})', get_project_employees, name="get_project_employees"),
	url(r'^list/task/(?P<task_id>[0-9]{0,2})/employees', list_task_employees, name="list_task_employees"),
	url(r'^list/tasks/todo/(?P<channel_id>[0-9]{0,2})', list_todo_tasks, name="list_todo_tasks"),
	url(r'^list/tasks/doing/(?P<channel_id>[0-9]{0,2})', list_done_tasks, name="list_doing_tasks"),
	url(r'^list/tasks/done/(?P<channel_id>[0-9]{0,2})', list_done_tasks, name="list_done_tasks"),
	url(r'^(?P<team_id>[A-Z0-9]{0,9})/list/projects', list_all_projects, name="list_all_projects"),
	url(r'^(?P<team_id>[A-Z0-9]{0,9})/list/managers', list_managers, name="list_managers"),
	url(r'^(?P<team_id>[A-Z0-9]{0,9})/list/employees', get_all_employees, name="get_all_employees"),
	url(r'^(?P<team_id>[A-Z0-9]{0,9})/list/(?P<employee_id>[0-9]{0,2})/task/(?P<task_id>[0-9]{0,2})/disjuncts/todo', list_todo_disjuncts, name="list_todo_disjuncts"),
	url(r'^(?P<team_id>[A-Z0-9]{0,9})/list/(?P<employee_id>[0-9]{0,2})/task/(?P<task_id>[0-9]{0,2})/disjuncts/doing', list_doing_disjuncts, name="list_doing_disjuncts"),
	url(r'^(?P<team_id>[A-Z0-9]{0,9})/list/(?P<employee_id>[0-9]{0,2})/task/(?P<task_id>[0-9]{0,2})/disjuncts/done', list_done_disjuncts, name="list_done_disjuncts"),
	url(r'^(?P<team_id>[A-Z0-9]{0,9})/list/(?P<employee_id>[0-9]{0,2})/task/(?P<task_id>[0-9]{0,2})/disjuncts/count', count_disjuncts_of_user, name="count_disjuncts_of_user"),
]
