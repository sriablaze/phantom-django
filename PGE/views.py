from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.core.mail import send_mail
from django.http import HttpResponse, HttpResponseRedirect
from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone


from rest_framework.parsers import JSONParser

from PGE.models import Absence, Discussion, Employee, Link, Manager, Preference, Priority, Pipe, Project, Role, Selection, Session, Task, Team, Disjunct, Stage, Workingproject
from PGE.serializers import DisjunctSerializer, EmployeeSerializer, ManagerSerializer, LinkSerializer, ProjectSerializer, TaskSerializer, TeamSerializer  
from PGE.tasks import notify_employees, notify_new_team

from datetime import datetime, timedelta, time


from dateutil import parser

from apiclient.discovery import build

from oauth2client.service_account import ServiceAccountCredentials

from PhantomGabEngine.settings import DEFAULT_FROM_EMAIL

from PGE.constants import CLIENT_ID
from PGE.constants import CLIENT_SECRET


import httplib2
import json
import pyrebase
import pytz
import os.path
import sys
import random
import requests




try:
    import apiai
except ImportError:
    sys.path.append(
        os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir)
    )
    import apiai

CLIENT_ACCESS_TOKEN = '75d2f175cd05473fbddba4d6475a49d8'
SESSION_ID = 1001

service_account_email = 'calendar@phantom-gab-engine.iam.gserviceaccount.com'

CLIENT_SECRET_FILE = 'PGE/calendar_service_account.json'

SCOPES = 'https://www.googleapis.com/auth/calendar'
scopes = [SCOPES]
tz = pytz.timezone('Asia/Kolkata')



config = {
  "apiKey": "AIzaSyB4555K4PmN7z5oMFIIfu08HSV_NRReSZQ",
  "authDomain": "phantom-gab-engine.firebaseapp.com",
  "databaseURL": "https://phantom-gab-engine.firebaseio.com",
  "storageBucket": "phantom-gab-engine.appspot.com",
  "serviceAccount": "PGE/phantom-gab-engine-firebase-adminsdk-o9tcv-6faea27d58.json"
}

firebase = pyrebase.initialize_app(config)
auth = firebase.auth()
firebase_user = auth.sign_in_with_email_and_password("sriablaze@gmail.com", "1234sri#")
db = firebase.database()

# Utility method to delete unicodes


CONTEXT_TASK_ASSIGNMENT = 'TASK_ASSIGNMENT'
MEETING_CALLBACK_ID = 'MEET/'
LEAVE_REQUEST_CALLBACK_ID = 'LEAVE/'


BUTTON_OPTION_YES = 'Yes'
BUTTON_OPTION_NO = 'No'

CONTEXTS_DELETE_ENDPOINT = 'https://api.api.ai/v1/contexts/{0}?sessionId={1}'
CONTEXTS_ENDPOINT = 'https://api.api.ai/v1/contexts?sessionId={0}'
OAUTH_SLACK_INITIATION_URL = 'https://slack.com/oauth/authorize?scope=identity.basic,identity.team,identity.avatar&client_id={0}'.format(CLIENT_ID)
DATE_PERIOD_KEY = 'date_period'
DATE_KEY = 'date'
GITHUB_BASE_ENDPOINT = 'https://api.github.com'
TODO_STAGE_NAME = 'TODO'
DOING_STAGE_NAME = 'DOING'
DONE_STAGE_NAME = 'DONE'
TASK_ADDITION_KEY_PROJECT_NAME = "project_name"
TASK_ADDITION_KEY_TASKS = "tasks"
TASK_ADDITION_MANAGER_EMAIL = "manager_email"
TASK_ENTITY_NAME = "Task"
DISJUNCT_ENTITY_NAME = "Disjunct"
EMPLOYEE_ENTITY_NAME = "employee"
PROJECT_ENTITY_NAME = "Project"
TASK_ENTITY_ADDITION_URL = "https://api.api.ai/v1/entities/{0}/entries?v=20150910".format(TASK_ENTITY_NAME)
DISJUNCT_ENTITY_ADDITION_URL = "https://api.api.ai/v1/entities/{0}/entries?v=20150910".format(DISJUNCT_ENTITY_NAME)
EMPLOYEE_ADDITION_URL = "https://api.api.ai/v1/entities/{0}/entries?v=20150910".format(EMPLOYEE_ENTITY_NAME)
PROJECT_ADDITION_URL = "https://api.api.ai/v1/entities/{0}/entries?v=20150910".format(PROJECT_ENTITY_NAME)
MESSAGE_SUBMISSION_URL = "https://api.api.ai/v1/query?v=20150910"
SUCCESS_STATUS_CODE = 200
MESSAGE_REQUEST_KEY = "message"
ACTION_INCOMPLETE = "actionIncomplete"
RESULT_KEY = "result"
DATE_DEADLINE_KEY = "date_deadline"
DURATION_DEADLINE_KEY = "duration_deadline"
headers = {'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer b55df5347afe4002a39e94cd61c121c9'}
WORK_START_INTENT_NAME = "work_start"
SESSION_START_INTENT_NAME = "session_start"
SESSION_END_INTENT_NAME = "session_end"
BREAK_START_INTENT_NAME = "break_start"
BREAK_END_INTENT_NAME = "break_end"
TASK_KEY = "task"
DISJUNCT_NAME_KEY = "Disjunct"
MEETING_INTENT_NAME = "meet_schedular"
ASSIGNMENT_INTENT_NAME = "Assignment"
ASSIGNMENT_CONTINUATION_INTENT_NAME = "assignment_continuation"
LEAVE_ABSENCE_REQUEST_INTENT = "set_absence"
LEAVE_REQUEST_ACCEPTANCE_INTENT = "leave_reply_acceptance"
LEAVE_REQUEST_DENIAL_INTENT = "leave_request_denial"
SET_DISCUSSION_PREFERENCE_INTENT = "discussion_preference"
DISCUSSION_SCHEDULE_REQUEST_INTENT = "discussion_schedule_request"
ADD_DISJUNCTS_TO_TASK_FORMAT_REQUEST = "create_disjuncts_to_task"
GROUP_DISJUNCT_ADDITIONS = "group_disjuncts_addition"
TASK_LIST_REQUEST = "task_list_request"
LIST_DISJUNCTS_REQUEST_TODO = 'list_disjuncts_request_to_do'
LIST_DISJUNCTS_REQUEST_DOING = 'list_disjuncts_request_doing'
SHIFT_DISJUNCT_REQUEST = 'shift_disjunct_request'
SCHEDULE_BREAKS_INTENT_NAME = 'schedule_breaks'
SET_WORK_FROM_HOME = 'set_work_from_home'
SWITCH_PROJECT_INTENT_NAME = "switch_project"


def log_user_in(work_type):
    print(work_type)



def build_service():
    credentials = ServiceAccountCredentials.from_json_keyfile_name(
        filename=CLIENT_SECRET_FILE,
        scopes=SCOPES
    )

    http = credentials.authorize(httplib2.Http())

    service = build('calendar', 'v3', http=http)

    return service


def create_event(deadline, summary, description):
    service = build_service()
    
    start_datetime = datetime.now(tz=tz)
    event = service.events().insert(calendarId='sricharanprograms@gmail.com', body={
        'summary': summary,
        'description': description,
        'start': {'dateTime': start_datetime.isoformat()},
        'end': {'dateTime': deadline.isoformat()},
    }).execute()

    print(event)



def byteify(input):
    if isinstance(input, dict):
        return {byteify(key): byteify(value)
                for key, value in input.items()}
    elif isinstance(input, list):
        return [byteify(element) for element in input]
    elif isinstance(input, bytes):
        return input.encode('utf-8')
    else:
        return input


def call_api(session_id, query):
    ai = apiai.ApiAI(CLIENT_ACCESS_TOKEN)

    request = ai.text_request()

    request.session_id = session_id

    request.query = query

    response = request.getresponse()

    return response.read()


def send_automated_message_to_nlp(message, employees):
    for employee in employees:
        print('At automation')
        print(employee.unique_id)
        request_dict = {"query" : [message], "sessionId" : employee.unique_id, "lang" : "en" , "timezone" : "Asia/Colombo"} 
        request_dict = json.dumps(request_dict)
        print(request_dict)
        response = requests.post(MESSAGE_SUBMISSION_URL, data=request_dict, headers=headers)
        response_dict = byteify(response.json())
        print(response_dict)


def add_user_to_agent(user_name):
    headers = {'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer b55df5347afe4002a39e94cd61c121c9'}
    request_dict = {"value" : user_name, "synonyms" : [user_name]}
    request_dict = json.dumps(request_dict)
    entity_request = requests.post(EMPLOYEE_ADDITION_URL, data=request_dict, headers=headers)
    print(entity_request.json())
    if entity_request.status_code == SUCCESS_STATUS_CODE:
        print("Entity added successfully")


def add_entries_to_agent(task_names):
    headers = {'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer b55df5347afe4002a39e94cd61c121c9'}
    request_list = []
    for task_name in task_names:
        request_dict = {"value" : task_name, "synonyms" : [task_name]}
        request_list.append(request_dict)
    request_list = json.dumps(request_list)
    entity_request = requests.post(DISJUNCT_ENTITY_ADDITION_URL, data=request_list, headers=headers)
    print(entity_request.json())
    if entity_request.status_code == SUCCESS_STATUS_CODE:
        print("Entity added successfully")

@csrf_exempt
def add_tasks(request):
    if request.method == 'POST':
        selection_dict = {}
        task_names = []
        recieved_json = json.loads(request.body)
        print(recieved_json)
        recieved_dict = byteify(recieved_json)
        channel_name = recieved_dict['channel_name']
        manager_email = recieved_json['manager_email']
        user = User.objects.get(email=manager_email)
        employee_obj = user.employee
        manager_obj, created = Manager.objects.get_or_create(employee_instance=employee_obj)
        print(created)
        print(Manager.objects.all())
        manager_obj.project_set.create(project_name=channel_name)
        print(manager_obj.project_set.all())
        project_obj = Project.objects.get(project_name=channel_name)
        print(recieved_dict)
        for task_obj in recieved_dict['tasks']:
            task_name = task_obj['task_name']
            task_names.append(task_name)
            project_obj.task_set.create(task_name=task_name)

        print(project_obj.task_set.all())
        for role_emp_object in recieved_dict["employees"]:
            role_name = role_emp_object['role_name']
            employee_email = role_emp_object['employee']['email']
            print(employee_email)
            role_obj = Role.objects.get(role_name=role_name)
            user = User.objects.get(email=employee_email)
            employee_obj = user.employee
            project_obj.employees.add(employee_obj)
            selection_obj, created = project_obj.selections.get_or_create(role=role_obj)
            selection_obj.employees.add(employee_obj)
            selection_dict[role_name] = selection_obj
        print(selection_obj.employees.count())
        print(selection_dict)
        for role, selection_obj in selection_dict.items():
            project_obj.selections.add(selection_obj)
        entity_entries = [] 
        entity_name = TASK_ENTITY_NAME
        request_list = []
        for task_name in task_names:
            request_dict = {"value" : task_name, "synonyms" : [task_name]}
            request_list.append(request_dict)

        print(request_list)       
        request_list = json.dumps(request_list)
        entity_request = requests.post(TASK_ENTITY_ADDITION_URL, data=request_list, headers=headers)
        print(entity_request.json())
        if entity_request.status_code == SUCCESS_STATUS_CODE:
            print("Entity added successfully")
            
        return HttpResponse(status=200)
    else:
        return HttpResponse(status=403)
        



def _close_session(user):
    session = Session.objects.get(end_date_time=None, user=user)
    session.end_date_time = datetime.now()
    session.save()
    

def _days_hours_minutes(td):
    return td.days, td.seconds//3600, (td.seconds//60)%60


def _format_end_session_response(days, hours, minutes, task_name):
    response_string = ""
    
    is_hour_data_present = False
    
    if hours != 0:
        is_hour_data_present = True
        if hours > 1:
            response_string = "{0} hours".format(hours)
        else:
            response_string = "{0} hour".format(hours)
    
    if minutes != 0:
        if is_hour_data_present:
            response_string = response_string + " " + "and"
        
        if minutes > 1:
            response_string = response_string + " " + "{0} minutes.".format(minutes)
        else:
            response_string = response_string + " " + "{0} minute.".format(minutes)



    response = "You've worked on {0} for {1}".format(task_name, response_string)
    response = {
        "speech_response" : response
    }
    return response

def send_response(results_dict):
    print("Im at the send reponse")
    fulfillment = results_dict['fulfillment']   
    speech_response = fulfillment['speech']
    response = {
        "speech_response" : speech_response
    }
    return response


def create_session(request, user, disjunct, if_end_break=False):
    user_last_session = Session.objects.filter(user=user).last()
    if user_last_session is not None and user_last_session.end_date_time is None:
        print("I shouldnt be here at all")
        user_last_session.end_date_time = datetime.now(tz=tz)
        end_session(request, user)
        logout(request)
    session = Session(user=user, disjunct=disjunct, start_date_time=datetime.now())
    login(request, user)
    if if_end_break is False:
        session.is_master_session = True
    session.save()
    print(session.id)
    

def end_session(request, user):
    _close_session(user)
    master_session = Session.objects.filter(user=user, is_master_session=True).last()
    master_session_id = master_session.id
    print(master_session_id)
    all_sessions = Session.objects.filter(user=user, id__gte=master_session_id)
    session_timedelta = timedelta(0, 0, 0)
    for session in all_sessions:
        print("The session id is")
        print(session.id)
        print(timezone.localtime(session.start_date_time))
        print(timezone.localtime(session.end_date_time))
        session_timedelta = session_timedelta + (timezone.locatime(session.end_date_time) - timezone.localtime(session.start_date_time))

    days, hours, minutes = _days_hours_minutes(session_timedelta)
    task_name = master_session.disjunct.disjunct_name
    response = _format_end_session_response(days, hours, minutes, task_name)
    logout(request)
    return response


def start_break(request, user):
    _close_session(user)
    

def end_break(request, user, channel_name):
    session = Session.objects.filter(is_master_session=True).last()
    disjunct = session.disjunct
    create_session(request, user, disjunct, if_end_break=True)


def set_discussion_preference(employee, preference_start_time, preference_end_time):
    preference, created = Preference.objects.get_or_create(employee=employee)
    preference.from_date_time = preference_start_time
    preference.to_date_time = preference_end_time
    preference.save()
    pref = employee.preference
    


def create_meeting_event(meet_time, summary, description, attendees=None):
    service = build_service()
    end_time = meet_time + timedelta(hours=1)
    event = service.events().insert(calendarId='sricharanprograms@gmail.com', body={
        'summary': summary,
        'description': description,
        'start': {'dateTime': meet_time.isoformat()},
        'end': {'dateTime': end_time.isoformat()},
        'attendees': attendees
    }).execute()

    print(event)


def schedule_meeting(meet_time, meet_venue, attendees_email_list, organizer_email):
    attendees = []
    meet_time = parser.parse(meet_time)
    meet_time = meet_time - timedelta(hours=5, minutes=30)
    meet_time = meet_time.astimezone(tz)
    print("The meet time is {0}".format(meet_time))
    for attendee_email in attendees_email_list:
        each_mail_dict  = {'email' : attendee_email}
    attendees.append(each_mail_dict)
    username = User.objects.get(email=organizer_email).username
    summary = "Meeting organized by {0}".format(username)
    description = "Weekly meetup"
    create_meeting_event(meet_time, summary, description, attendees)


def create_attendence_tracker():
    managers = Manager.objects.all()
    for manager in managers:
        manager_email = manager.employee_instance.user.email
        at_stripped_email = manager_email.replace("@", "")
        dot_stripped_email = at_stripped_email.replace(".", "")
        input_dict = {"name" : "Phantom Attendence Tracker"}
        unique_key = "PAT{0}".format(dot_stripped_email) 
        db.child("master").child("channels").child(unique_key).set(input_dict, firebase_user['idToken'])
        channel_dict = {"channel_id" : unique_key}
        db.child("master").child(dot_stripped_email).child("associated_rooms").push(channel_dict, firebase_user['idToken'])
        

def leave_request(request, start_date, end_date, manager_email, user):
    at_stripped_email = manager_email.replace("@", "")
    dot_stripped_email = at_stripped_email.replace(".", "")
    unique_key = "PAT{0}".format(dot_stripped_email)
    username = user.username
    days, minutes, hours = _days_hours_minutes(end_date - start_date)
    message_text = "Hey, {0} has requested leave of absence for {1} days from {2} to {3}".format(user.first_name, days + 1, str(start_date), str(end_date))
    message_dict = {"senderId" : "phantom", "senderName" : "phantom", "text": message_text}
    channel_dict = db.child("master").child("channels").child(unique_key).child("messages").push(message_dict, firebase_user['idToken'])
    

def return_min_and_max_date_tuples(date_tuple_one, date_tuple_two, index=0):
    if date_tuple_one[index].time() > date_tuple_two[index].time():
        return date_tuple_two, date_tuple_one
    elif date_tuple_two[index].time() > date_tuple_one[index].time():
        return date_tuple_one, date_tuple_two
    else:
        return date_tuple_one, date_tuple_two


def find_intersection(date_range_list):
    is_full_intersection = True

    intersection_tuple = date_range_list[0]
    for date_tuple in date_range_list[1:]:
        
        min_tuple, max_tuple = return_min_and_max_date_tuples(intersection_tuple, date_tuple)
        if max_tuple[0] < min_tuple[1]:
            start_date_time = max_tuple[0]
            min_end_date_tuple, max_end_date_tuple = return_min_and_max_date_tuples(min_tuple, max_tuple, index=1)
            min_date_time = min_end_date_tuple[1]
            intersection_tuple = (start_date_time, min_date_time)
        else:
            is_full_intersection = False
            break
    
    return (is_full_intersection, intersection_tuple)



# Basically we apply the nCr formula where we apply the r in reverse order
def generate_combinations(request, index_list,  n,  r, index, combination, i, date_range_list):
    
    set_preference = request.session['set_preference']

    if index == r:
        date_tuple_list = []
        
        if not set_preference:
            for combination_index in combination:
                date_tuple_list.append(date_range_list[combination_index])
            
            is_intersecting, intersection_tuple = find_intersection(date_tuple_list)
            request.session['set_preference'] = is_intersecting
            
            if request.session['set_preference']:
                request.session['intersection_tuple'] = intersection_tuple
        return
        


    if i >= n:
        return 
    
    combination[index] = index_list[i]

    generate_combinations(request, index_list, n, r, index + 1, combination, i + 1, date_range_list)
    generate_combinations(request, index_list, n, r, index, combination, i + 1, date_range_list)

    

def intersection_helper(request, date_range_list):
    complete_intersection = False
    request.session['intersection_tuple'] = (0, 0)
    request.session['set_preference'] = False

    date_tuple_list = []
    size = len(date_range_list)
    index_list = [i for i in range(0, size)]
    r = size
    while not request.session['set_preference'] and r > 1:
        combination_list = [0 for i in range(0, r)]
        generate_combinations(request, index_list, size, r, 0, combination_list, 0, date_range_list)
        r = r - 1
    if r == size - 1:
        complete_intersection = True

    if request.session['set_preference']:
        print("I am here")
        return (request.session['intersection_tuple'], complete_intersection)
    else:
        return (None, complete_intersection)


def render_interactive_request_for_slack(text, title, callback_id):
    text = text
    actions = [
        {
            "name": "Yes",
            "text": "Yes",
            "type": "button",
            "value": "Yes"
        },
        {
            "name": "No",
            "text": "No",
            "type": "button",
            "value": "No"
        }
         
    ]
    title = title
    callback_id = callback_id
    attachment_type = 'default'
    inner_response_dict = {'title':title, 'callback_id':callback_id, 'attachment_type':attachment_type, 'actions':actions}
    outer_response_dict = {'text': text, 'attachments':[inner_response_dict]}
    return outer_response_dict


@csrf_exempt
def handle_message(request):
    if request.method == 'GET':
        return HttpResponse("Reached server")
    if request.method == 'POST':
        recieved_json = json.loads(request.body)
        input_dict = byteify(recieved_json)
        print(input_dict)
        from_node = input_dict['from_node']
        message_dict = input_dict['message']
        channel_id = message_dict['channel']
        team_id = message_dict['team']
        if from_node:
            team_id = message_dict['team']
            user_id = message_dict['user']
            employee_existence = Employee.objects.filter(unique_id=user_id).exists()
            if not employee_existence:
                print("I am now here")
                access_token = Team.objects.get(team_id=team_id).access_token
                response = requests.get('https://slack.com/api/users.info?token={0}&user={1}&pretty=1'.format(access_token, user_id))
                response_dict = byteify(response.json())
                print(response_dict)
                user_dict = response_dict['user']
                username = user_dict['name']
                is_bot = user_dict['is_bot']
                user_email = user_dict['profile']['email']
                if not is_bot:
                    add_user_to_agent(user_id)
                    user = User.objects.create_user(username=user_id, email=user_email, first_name=username)
                    employee = Employee(user=user, unique_id=user_id, slack_bot_channel_id=channel_id)
                    employee.save()
            else:
                employee = Employee.objects.get(unique_id=user_id)
                if employee.slack_bot_channel_id is None:
                     employee.slack_bot_channel_id = channel_id
                     employee.save()
                user_email = employee.user.email

            message_text = input_dict['message']['text']
            open_angular_stripped = message_text.replace("<", "")
            the_stripped_message = open_angular_stripped.replace(">", "")
            print(the_stripped_message)
        # message = input_dict[MESSAGE_REQUEST_KEY]
        # channel_name = input_dict['channel_name']
        #user_email = input_dict.get('email', None)
        #message = message.lstrip()
        #message = message[8:]
        
        try:
            channel_name = employee.workingproject.project.project_name
        
        except Workingproject.DoesNotExist:
            
            default_project = employee.project_set.all().first()
            if default_project is None:
                response = {
                    'speech_response' : "You are not associated with any projects."
                }
            else:
                working_project = Workingproject(project=default_project, employee=employee)
                working_project.save()
                response = {
                    'speech_response' : "Please use [switch project_name] command to select the project that you are currently working on."
                }
            return HttpResponse(json.dumps(response), content_type='application/json')
        
        message = the_stripped_message.replace("@", "")
        print(message)
        headers['Authorization'] = 'Bearer {0}'.format(CLIENT_ACCESS_TOKEN)
        print('The session id of the employee is')
        print(employee.unique_id)
        request_dict = {"query" : [message], "sessionId" : employee.unique_id, "lang" : "en" , "timezone" : "Asia/Colombo"} 
        request_dict = json.dumps(request_dict)
        print('The request dict is')
        print(request_dict)
        response = requests.post(MESSAGE_SUBMISSION_URL, data=request_dict, headers=headers)
        response_dict = byteify(response.json())
        results_dict = response_dict[RESULT_KEY]
        print(results_dict)
        intent_name = results_dict['metadata']['intentName']
        result_parameters = results_dict["parameters"]
        action_incomplete = results_dict[ACTION_INCOMPLETE]
        user = User.objects.get(username=user_id)
        print(user)
        if intent_name == WORK_START_INTENT_NAME:
            method_call = eval(results_dict['action'])
            work_type = result_parameters['work_types']
            method_call(work_type)
            response = send_response(results_dict)

        
        elif intent_name == SESSION_START_INTENT_NAME:
            if action_incomplete is False:
                disjunct_name = result_parameters[DISJUNCT_NAME_KEY]
                disjunct = Disjunct.objects.get(employee=employee, disjunct_name=disjunct_name)
                the_associated_task = disjunct.task
                stage, created = Stage.objects.get_or_create(stage_name=DOING_STAGE_NAME)
                disjunct.stage = stage
                the_associated_task.stage = stage
                disjunct.save() 
                the_associated_task.save()
                create_session(request, employee.user, disjunct)

                
        elif intent_name == BREAK_START_INTENT_NAME:
            method_call = eval(results_dict['action'])
            # return_time = result_parameters['return_time']
            method_call(request, user)


        elif intent_name == BREAK_END_INTENT_NAME:
            method_call = eval(results_dict['action'])(request, user, channel_name)

        elif intent_name == SESSION_END_INTENT_NAME:
            response = eval(results_dict['action'])(request, user)
            return HttpResponse(json.dumps(response), content_type="application/json")


        elif intent_name == SET_WORK_FROM_HOME:
            if action_incomplete is False:
                absence_parameter = result_parameters['absence_parameter']
                date = parser.parse(absence_parameter['date'])
                
                
        elif intent_name == LEAVE_ABSENCE_REQUEST_INTENT:
            absence_dict = result_parameters['absence']
            if DATE_PERIOD_KEY in absence_dict.keys():
                date_range = absence_dict[DATE_PERIOD_KEY]
                for (index, character) in enumerate(date_range):
                    if character == '/':
                        start_date = parser.parse(date_range[0 : index])
                        end_date = parser.parse(date_range[index + 1 :])
                        break 
            else:
                leave_date = parser.parse(absence_dict[DATE_KEY])
                start_date = leave_date
                end_date = leave_date

            print(start_date)
            print(end_date)
            absence = Absence(employee=employee, start_date=start_date, end_date=end_date)
            absence.save()
            absence_id = absence.id
            project_obj = Project.objects.get(project_name=channel_name)
            manager = project_obj.manager
            print(manager.employee_instance)
            multicast_dict = {}
            multicast_list = []
            days, hours, minutes = _days_hours_minutes(end_date - start_date)
            # leave_request(request, start_date, end_date, manager_email, employee)

            if days == 0:
                response = "Your leave request for {0} has been forwarded to your manager for approval".format(str(start_date.strftime('%Y-%m-%d')))
                response_for_manager = "Hey, @{0} has requested for leave on {1}".format(employee.user.first_name, str(start_date.strftime('%Y-%m-%d')))
            
            else:
                response = "Your leave request for {0} days from {1} to {2} has been forwarded to your manager ".format(days + 1, str(start_date.strftime('%Y-%m-%d')), str(end_date.strftime('%Y-%m-%d')))
                response_for_manager = "Hey, @{0} has requested leave of absence for {1} days from {2} to {3}".format(employee.user.first_name, days + 1, str(start_date.strftime('%Y-%m-%d')), str(end_date.strftime('%Y-%m-%d')))
            
            title_for_interaction = "Are you okay with the absence?"
            response_for_manager = render_interactive_request_for_slack(response_for_manager, title_for_interaction, LEAVE_REQUEST_CALLBACK_ID + str(absence_id))
            print(response_for_manager)
            multicast_dict[manager.employee_instance.slack_bot_channel_id] = response_for_manager
            multicast_list.append(multicast_dict)

            response = {
                "speech_response" : response,
                "multicast_list" : multicast_list,
                "team_id" : team_id
            }

            return HttpResponse(json.dumps(response), content_type="application/json")


        elif intent_name == LEAVE_REQUEST_ACCEPTANCE_INTENT:
            employee_name = result_parameters["employee"]
            user = User.objects.get(username=employee_name)
            recipient_email = user.email
            leave_req = user.absence_set.filter(is_approved=False).last()
            leave_req.is_approved = True
            response = "Your leave request is accepted by your manager, Have a great holiday!"
            at_stripped_email = recipient_email.replace("@", "")
            dot_stripped_email = at_stripped_email.replace(".", "")
            unique_key = "PAT{0}".format(dot_stripped_email)
            message_dict = {"senderId" : "phantom", "senderName" : "phantom", "text": response}
            db.child("master").child("channels").child(unique_key).child("messages").push(message_dict, firebase_user['idToken'])


        elif intent_name == DISCUSSION_SCHEDULE_REQUEST_INTENT:
            employee_ids = result_parameters['employee']
            task_name = result_parameters.get('Task', None)
            date = result_parameters.get('date', None)
            date_range_list = []
            employee_date_tuple_index_dict = {}
            if date:
                discussion_request_date = parser.parse(date)
            else:
                discussion_request_date = datetime.now().date() 
            index = 0
            
            # add the id of the employee who scheduled the meeting
            print(employee)
            employee_ids.append(employee.unique_id)
            print(employee_ids)
            multicast_dict = {}
            for employee_id in employee_ids:
                employee_obj = Employee.objects.get(unique_id=employee_id)
                discussion_preference = employee_obj.preference
                multicast_dict[employee_obj.slack_bot_channel_id] = None
                if discussion_preference is not None: 
                    formatted_start_date_time = timezone.localtime(discussion_preference.from_date_time)
                    formatted_end_date_time = timezone.localtime(discussion_preference.to_date_time)
                    print(formatted_start_date_time)
                    if formatted_start_date_time.date() == discussion_request_date:
                        date_tuple = (formatted_start_date_time, formatted_end_date_time)
                        date_range_list.append(date_tuple)
                        employee_date_tuple_index_dict[employee_obj] = index
                        index = index + 1
            print(date_range_list)
            intersection_tuple, full_intersection = intersection_helper(request, date_range_list)
            if intersection_tuple is not None:
                start_time = intersection_tuple[0]
                start_date_time_str = str(start_time)
                request.session['intersection_tuple'] = None
                if full_intersection:
                    response = "I have scheduled your discussion at {0}".format(start_date_time_str)
                    for key in multicast_dict:
                        multicast_dict[key] = response
                    del multicast_dict[employee.slack_bot_channel_id]
                else:
                    response = "I have found an optimal time for the discussion at {0}, please schedule manually".format(start_date_time_str)
                    multicast_dict = None

            else:
                response = "I am not able to schedule an optimal discussion time for you!"
                multicast_dict = None
                
            print(multicast_dict)
            response = {
                "speech_response": response,
                "multicast_dict" : multicast_dict,
                "team_id" : team_id
            }
            return HttpResponse(json.dumps(response), content_type="application/json")


        elif intent_name == MEETING_INTENT_NAME:
            if action_incomplete is False:
                attendees_email_list = []
                meet_time = result_parameters['date-time']
                meet_venue = result_parameters['meet-locations']
                meet_entities = result_parameters['meetEntity']
                all_roles = Role.objects.all()
                role_names = []
                for role in all_roles:
                    role_names.append(role.role_name)
                project_obj = Project.objects.get(project_name=channel_name)
                for meet_entity in meet_entities:
                    # Belongs to a role group
                    if meet_entity in role_names:
                        selection_obj = project_obj.selections.get(role__role_name=meet_entity)
                        employees = selection_obj.employees.all()
                        for employee in employees:
                            email = employee.user.email
                            attendees_email_list.append(email)
                    # An individual meet entity
                    else:
                        email = Employee.objects.get(user__username=meet_entity).user.email
                        attendees_email_list.append(email)
                print(attendees_email_list)
                eval(results_dict['action'])(meet_time, meet_venue, attendees_email_list, user_email)

        # intent handling the user preference
        elif intent_name == SET_DISCUSSION_PREFERENCE_INTENT:
            
            preferred_time_interval = result_parameters['date-time']
            for (index, character) in enumerate(preferred_time_interval):
                if character == '/':
                    start_date_time = parser.parse(preferred_time_interval[0 : index])
                    end_date_time = parser.parse(preferred_time_interval[index + 1 :])  
                    break
            start_hour = start_date_time.hour
            start_minute = start_date_time.minute
            formatted_start_time = time(hour=start_hour, minute=start_minute)
            start_date = start_date_time.date()
            start_date_time = datetime.combine(start_date, formatted_start_time)
            end_hour = end_date_time.hour
            end_minute = end_date_time.minute
            formatted_end_time = time(hour=end_hour, minute=end_minute)
            end_date = end_date_time.date()
            end_date_time = datetime.combine(end_date, formatted_end_time)
            eval(results_dict['action'])(employee, start_date_time, end_date_time)
        
        
        elif intent_name == GROUP_DISJUNCT_ADDITIONS:
            if action_incomplete == False:
                disjunct_names = result_parameters['any']
                task_name = result_parameters['task']
                project_obj = Project.objects.get(project_name=channel_name)
                task = Task.objects.get(task_name=task_name, project=project_obj)
                default_stage = Stage.objects.get(stage_name=TODO_STAGE_NAME)
                for disjunct_name in disjunct_names:
                    disjunct = Disjunct(disjunct_name=disjunct_name, employee=employee, task=task, stage=default_stage)
                    disjunct.save()
                add_entries_to_agent(disjunct_names)
                context_name = 'considered_task'
                session_id = employee.unique_id
                url = CONTEXTS_DELETE_ENDPOINT.format(context_name, session_id)
                print(url)
                context_delete_request = requests.delete(CONTEXTS_DELETE_ENDPOINT.format(context_name, session_id), headers=headers)
                print(context_delete_request.json())

        
        elif intent_name == SHIFT_DISJUNCT_REQUEST:
            disjunct_names = result_parameters['disjuncts']
            for disjunct_name in disjunct_names:
                disjunct_obj = Disjunct.objects.get(disjunct_name=disjunct_name, employee=employee)
                stage_id = disjunct_obj.stage.id
                stage = Stage.objects.get(id=stage_id + 1)
                disjunct_obj.stage = stage
                disjunct_obj.save()
                print(disjunct_obj.stage.id)
                print("shifted")
                

        elif intent_name == TASK_LIST_REQUEST:
            task_name_list_to_be_printed = []
            tasks = employee.workingproject.project.task_set.all()
            if not tasks:
                response = "There are no tasks assigned to you in your currently working project."
                response = {
                    "speech_response": response
                }
                return HttpResponse(json.dumps(response), content_type="application/json")
            the_header_string = "Your tasks are"
            task_name_list_to_be_printed.append(the_header_string)
            for (index, task) in enumerate(tasks):
                sr_no = index + 1
                the_new_list_element = str(sr_no) + '.' + " " + task.task_name  
                task_name_list_to_be_printed.append(the_new_list_element)
            
            the_list_string = '\n'.join(task_name_list_to_be_printed)
            
            response = the_list_string

            response = {
                "speech_response": response
            }

            return HttpResponse(json.dumps(response), content_type="application/json")


        
        elif intent_name == LIST_DISJUNCTS_REQUEST_TODO:
            task_name_list_to_be_printed = []
            todo_disjuncts = Disjunct.objects.filter(employee=employee, stage__stage_name=TODO_STAGE_NAME, task__project__project_name=channel_name)
            the_header_string = "The disjuncts/subtasks to do are"
            task_name_list_to_be_printed.append(the_header_string)
            for (index, disjunct) in enumerate(todo_disjuncts):
                sr_no = index + 1
                the_new_list_element = str(sr_no) + '.' + " " + disjunct.disjunct_name  
                task_name_list_to_be_printed.append(the_new_list_element)
            
            the_list_string = '\n'.join(task_name_list_to_be_printed)
            
            response = the_list_string

            response = {
                "speech_response": response
            }

            return HttpResponse(json.dumps(response), content_type="application/json")


        elif intent_name == LIST_DISJUNCTS_REQUEST_DOING:
            task_name_list_to_be_printed = []
            todo_disjuncts = Disjunct.objects.filter(employee=employee, stage__stage_name=DOING_STAGE_NAME, task__project__project_name=channel_name)
            the_header_string = "The disjuncts/subtasks that you're working on are"
            task_name_list_to_be_printed.append(the_header_string)
            for (index, disjunct) in enumerate(todo_disjuncts):
                sr_no = index + 1
                the_new_list_element = str(sr_no) + '.' + " " + disjunct.disjunct_name  
                task_name_list_to_be_printed.append(the_new_list_element)
            
            the_list_string = '\n'.join(task_name_list_to_be_printed)
            
            response = the_list_string

            response = {
                "speech_response": response
            }

            return HttpResponse(json.dumps(response), content_type="application/json")


        # Schedule breaks intent
        elif intent_name == SCHEDULE_BREAKS_INTENT_NAME:
            date_time_for_break = parser.parse(result_parameters['time'])
            date_time_for_break = tz.localize(date_time_for_break)
            # print(date_time_for_break)
            session = Session.objects.filter(user=employee.user).last()
            if session.end_date_time is None:
                message = "Hey, It's time for a break."
                break_message = "Please tell  if you are actually "
                channel_list = [employee.slack_bot_channel_id]
                notify_employees.apply_async(args=[message, channel_list, team_id], eta=date_time_for_break)
            else:
                response = {
                    'speech_response' : 'Sorry, You are not tracking work, Please track one and issue a break command.' 
                }
                return HttpResponse(json.dumps(response), content_type='application/json')

        # currently working here
        elif intent_name == SWITCH_PROJECT_INTENT_NAME:
            project_name = result_parameters['project_name']
            project = Project.objects.get(team__team_id=team_id, project_name=project_name)
            employee.workingproject.project = project
            employee.workingproject.save()
            
        elif intent_name == ASSIGNMENT_INTENT_NAME or intent_name == ASSIGNMENT_CONTINUATION_INTENT_NAME:
            if action_incomplete is False:
                employees = result_parameters["employee"]
                print(employees)
                task_name = result_parameters["task"]
                print(task_name)
                task_obj = Task.objects.get(task_name=task_name, project__project_name=channel_name)
                project_obj = Project.objects.get(project_name=channel_name)
                for employee_name in employees:
                    if project_obj.selections.filter(employees__user__username=employee_name).exists():
                        employee_obj = Employee.objects.get(user__username=employee_name)
                        task_obj.employees.add(employee_obj)
                    else:
                        response = "The employee {0} is not a valid candidate for task assignment".format(employee_name)
                        response = {
                            "speech_response": response
                        }
                        return HttpResponse(json.dumps(response), content_type="application/json")
                
                deadline_dict = result_parameters.get("deadline", None)
                if deadline_dict is not None:
                    date_duration = deadline_dict.get(DATE_DEADLINE_KEY, None)
                    duration_deadline = deadline_dict.get(DURATION_DEADLINE_KEY, None)
                    if duration_deadline is not None:
                        amount = duration_deadline["amount"]
                        unit = duration_deadline["unit"]
                        if unit == "day":
                            deadline = datetime.now(tz=tz).date() + timedelta(days=int(amount))
                    else:
                        deadline = parser.parse(date_duration)
                else:
                    deadline = task_obj.deadline
                
                summary = task_name
                description = "Task Deadline set by channel - {0}".format(channel_name)
                default_time = datetime.now(tz=tz).time()
                datetime_obj = datetime.combine(deadline, default_time)
                datetime_obj = tz.localize(datetime_obj)
                create_event(datetime_obj, summary, description)
                task_obj.deadline = deadline
                task_obj.save()
            else:
                pass
        response = send_response(results_dict)
        return HttpResponse(json.dumps(response), content_type="application/json")
        

@csrf_exempt
def get_employees(request, role='WFD'):
    if request.method == 'GET':
        print(role)
        queryset = Employee.objects.filter(priority__role__role_name=role).order_by('priority__magnitude', 'user__username')
        employee_serializer = EmployeeSerializer(queryset, many=True)
        print(employee_serializer.data)
        return JsonResponse(employee_serializer.data, status=201, safe=False)


@csrf_exempt
def get_channels(request, email):
    if request.method == 'GET':
        print("Im at the getChannels API")
        employee = Employee.objects.get(user__email=email)
        projects = employee.project_set.all()
        project_serializer = ProjectSerializer(projects, many=True)
        return JsonResponse(project_serializer.data, status=200, safe=False)


@csrf_exempt    
def list_links(request, channel_name):
    if request.method == 'GET':
        project = Project.objects.get(project_name=channel_name)
        queryset = project.link_set.all()
        link_serializer = LinkSerializer(queryset, many=True)
        return JsonResponse(link_serializer.data, status=200, safe=False)


@csrf_exempt
def add_employee(request):
    if request.method == 'POST':
        roles = []
        role_keys = ["1", "2", "3"]
        recieved_json = json.loads(request.body)
        recieved_dict = byteify(recieved_json)
        name = recieved_dict["name"]
        email = recieved_dict["email"]
        at_stripped_email = email.replace("@", "")
        dot_stripped_email = at_stripped_email.replace(".", "")
        input_dict = {"name": name}
        db.child("master").child(dot_stripped_email).set(input_dict, firebase_user['idToken'])
        for priorities in role_keys:
            roles.append(recieved_dict.get(priorities, None))
        print(roles)
        for(index, role) in enumerate(roles):
            if role is not None:
                role_obj = Role.objects.get(role_name=role)
                priority_obj = Priority.objects.get(role=role_obj, magnitude=index + 1)
                user = User.objects.create(username=name, email=email)
                employee_obj = Employee(user=user)
                employee_obj.save()
                employee_obj.priority.add(priority_obj)
        return HttpResponse(status=200)

@csrf_exempt
def submit_link_data(request):
    if request.method == 'POST':
        link_name = request.POST['linkName']
        selected_project = request.POST['projectList']
        url = request.POST['url']
        project = Project.objects.get(project_name=selected_project)
        link = Link(project=project, link_name=link_name, url=url)
        link.save()
        print(link)
        return HttpResponse(status=200) 


def _handle_slack_interaction_for_absence(callback_id, actions, manager_channel_id):
    for index, character in enumerate(callback_id):
        if character == '/':
            the_absence_id = callback_id[index + 1 :]
            break
    absence = Absence.objects.get(id=int(the_absence_id))
    employee_channel_id = absence.employee.slack_bot_channel_id
    
    singular_response = "Your manager is {0} with your absence on {1}"
    plural_response = "Your manager is {0} with your absence for {1} days from {2} to {3}"

    start_date = absence.start_date
    end_date = absence.end_date
    days, hours, minutes = _days_hours_minutes(end_date - start_date)

    if days == 0:
        response = singular_response
    else:
        response = plural_response
    
    
    is_yes_button = False

    for action_dict in actions:
        if action_dict['name'] == BUTTON_OPTION_YES:
            is_yes_button = True
            absence.is_approved = True
        else:
            absence.is_approved = False

        if response == singular_response:
            if is_yes_button:
                response = response.format(str('okay'), str(absence.start_date.strftime('%Y-%m-%d')))
            else:
                response = response.format(str('not okay'), str(absence.start_date.strftime('%Y-%m-%d')))
        else:
            if is_yes_button:
                response = response.format(str('okay'), str(days + 1), str(absence.start_date.strftime('%Y-%m-%d')), str(absence.end_date.strftime('%Y-%m-%d')))
            else:
                response = response.format(str('not okay'), str(days + 1), str(absence.start_date.strftime('%Y-%m-%d')), str(absence.end_date.strftime('%Y-%m-%d')))
    
    absence.save()
    response_dict = {}
    response_dict[employee_channel_id] = response
    return response_dict 
    

@csrf_exempt
def handle_slack_interaction(request):
    if request.method == 'POST':
        recieved_json = json.loads(request.body)
        recieved_dict = byteify(recieved_json)
        actions = recieved_dict['actions']
        callback_id = recieved_dict['callback_id']
        channel_id = recieved_dict['channel']['id']
        if LEAVE_REQUEST_CALLBACK_ID in callback_id:
            response_dict = _handle_slack_interaction_for_absence(callback_id, actions, channel_id)
            response = {
                'multicast_dict' : response_dict
            }
        print(response)
        return HttpResponse(json.dumps(response), content_type="application/json") 


@csrf_exempt
def handle_github_hook(request):
    if request.method == 'POST':
        user_email_list = ['sricharanprograms@gmail.com', 'sricharan14312@cse.ssn.edu.in']
        recieved_json = json.loads(request.body)
        recieved_dict = byteify(recieved_json)
        action = recieved_dict['action']
        pull_request_dict = recieved_dict['pull_request']
        user_dict = pull_request_dict['user']
        creator_login_id = user_dict['login']
        response = requests.get(GITHUB_BASE_ENDPOINT + '/users/' + creator_login_id)
        github_user_detail_dict = byteify(response.json())
        email = github_user_detail_dict.get('email', None)
        if email is None:
            creator_email = random.choice(user_email_list)
            creator_employee = Employee.objects.get(user__email=creator_email)
        else:
            creator_email = email
        
        github_user_detail_request_dict = {}
        assignee_list = pull_request_dict['assignees']
        for assignee_dict in assignee_list:
            login_id = assignee_dict['login']
            github_user_detail_request_dict[login_id] = None
        for login_id in github_user_detail_request_dict:
            response = requests.get(GITHUB_BASE_ENDPOINT + '/users/' + login_id)
            github_user_detail_dict = byteify(response.json())
            email = github_user_detail_request_dict.get('email', None)
            if email is None:
                email = random.choice(user_email_list)
                assigning_employee = Employee(user__email=email)
                pipe = Pipe.objects.get_or_create(mentor=assigning_employee, mentee=creator_employee)


        return HttpResponse(status=200)


def list_all_projects(request, team_id):
    if request.method == 'GET':
        team = Team.objects.get(team_id=team_id)
        queryset = team.project_set.all()
        serializer = ProjectSerializer(queryset, many=True)
        return JsonResponse(serializer.data, safe=False)


@csrf_exempt
def get_project_employees(request, channel_id):
    if request.method == 'GET':
        list_of_employees_serialized = []
        project_obj = Project.objects.get(id=int(channel_id))
        '''
        selections = project_obj.selections.all()
        for selection in selectionsg:
            queryset = selection.employees.all()
            employee_serializer = EmployeeSerializer(queryset, many=True)
            list_of_employees_serialized.append(employee_serializer.data)
        '''
        queryset = project_obj.employees.all()
        employee_serializer = EmployeeSerializer(queryset, many=True)
        return JsonResponse(employee_serializer.data, status=201, safe=False)



@csrf_exempt
def get_projects_of_employee(request, employee_id):
    if request.method == 'GET':
        employee = Employee.objects.get(id=int(employee_id))
        queryset = employee.project_set.all()
        employee_serializer = ProjectSerializer(queryset, many=True)
        return JsonResponse(employee_serializer.data, status=201, safe=False)


@csrf_exempt
def list_managers(request, team_id):
    if request.method == 'GET':
        queryset = []
        team = Team.objects.get(team_id=team_id)
        managers = Manager.objects.all()
        for manager in managers:
            teams_of_employee = manager.employee_instance.team.all()
            if team in teams_of_employee:
                queryset.append(manager)
        serializer = ManagerSerializer(queryset, many=True)
        return JsonResponse(serializer.data, safe=False)


@csrf_exempt
def get_all_employees(request, team_id):
    if request.method == 'GET':
        team = Team.objects.get(team_id=team_id)
        queryset = team.employee_set.all()
        employee_serializer = EmployeeSerializer(queryset, many=True)
        return JsonResponse(employee_serializer.data, status=201, safe=False)


# project creation api

@csrf_exempt
def create_project(request, team_id):
    if request.method == 'POST':
        recieved_json = json.loads(request.body)
        recieved_dict = byteify(recieved_json)
        # project specific details

        team = Team.objects.get(team_id=team_id)
        project_name = recieved_dict['project_name']
        manager_id = recieved_dict['manager_id']
        deadline = parser.parse(recieved_dict['deadline'])  
        employee_id_list = recieved_dict['employee_id_list']

        manager_instance = Manager.objects.get(id=int(manager_id))
        project = Project(project_name=project_name, manager=manager_instance, end_date=deadline, team=team)
        project.save()
        for emp_id in employee_id_list:
            employee = Employee.objects.get(id=int(emp_id))
            project.employees.add(employee)
        
        headers = {'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer b55df5347afe4002a39e94cd61c121c9'}
        request_dict = {"value" : project_name, "synonyms" : [project_name]}
        request_dict = json.dumps(request_dict)
        entity_request = requests.post(PROJECT_ADDITION_URL, data=request_dict, headers=headers)
        print(entity_request.json())
        response = {
            'project_id' : project.id
        }

        return HttpResponse(json.dumps(response), content_type="application/json")

    if request.method == 'GET':
        return HttpResponse(status=403)

@csrf_exempt
def handle_queries(request):
    recieved_json = json.loads(request.body)
    recieved_dict = byteify(recieved_json)
    name = recieved_dict['name']
    email = recieved_dict['email']
    phone = recieved_dict['phone']
    company_name = recieved_dict['company_name']
    description = recieved_dict['description']
    subject = 'Query from {0}'.format(name)
    the_message_list = [description]
    contact_detail = 'Contact {0} at {1} or {2}'.format(name, phone, email)
    the_message_list.append(contact_detail)
    recipient = 'rithwinsiva@gmail.com'
    the_message_string = '\n'.join(the_message_list)
    send_mail(subject, the_message_string, DEFAULT_FROM_EMAIL, [recipient], fail_silently=False)
    return HttpResponse(status=200)

# Tasks API

def list_todo_tasks(request, channel_id):
    project = Project.objects.get(id=int(channel_id))
    queryset  = Task.objects.filter(stage__stage_name=TODO_STAGE_NAME, project=project)
    task_serializer = TaskSerializer(queryset, many=True)
    return JsonResponse(task_serializer.data, status=201, safe=False)    


def list_doing_tasks(request, channel_id):
    project = Project.objects.get(id=int(channel_id))
    queryset  = Task.objects.filter(stage__stage_name=DOING_STAGE_NAME, project=project)
    task_serializer = TaskSerializer(queryset, many=True)
    return JsonResponse(task_serializer.data, status=201, safe=False)


def list_done_tasks(request, channel_id):
    project = Project.objects.get(id=int(channel_id))
    queryset  = Task.objects.filter(stage__stage_name=DONE_STAGE_NAME, project=project)
    task_serializer = TaskSerializer(queryset, many=True)
    return JsonResponse(task_serializer.data, status=201, safe=False)


def list_task_employees(request, task_id):
    task = Task.objects.get(id=int(task_id))
    queryset = task.employees.all()
    employee_serializer = EmployeeSerializer(queryset, many=True)
    return JsonResponse(employee_serializer.data, status=201, safe=False)   


# Disjunct API 

def list_todo_disjuncts(request, team_id, task_id, employee_id):
    queryset = Disjunct.objects.filter(task__id=int(task_id), employee__id=int(employee_id), stage__stage_name=TODO_STAGE_NAME)
    disjunct_serializer = DisjunctSerializer(queryset, many=True)
    return JsonResponse(disjunct_serializer.data, status=201, safe=False)    


def list_doing_disjuncts(request, team_id, task_id, employee_id):
    queryset = Disjunct.objects.filter(task__id=int(task_id), employee__id=int(employee_id), stage__stage_name=DOING_STAGE_NAME)
    disjunct_serializer = DisjunctSerializer(queryset, many=True)
    return JsonResponse(disjunct_serializer.data, status=201, safe=False) 


def list_done_disjuncts(request, team_id, task_id, employee_id):
    queryset = Disjunct.objects.filter(task__id=int(task_id), employee__id=int(employee_id), stage__stage_name=DONE_STAGE_NAME)
    disjunct_serializer = DisjunctSerializer(queryset, many=True)
    return JsonResponse(disjunct_serializer.data, status=201, safe=False) 


def count_disjuncts_of_user(request, team_id, task_id, employee_id):
    count = Disjunct.objects.filter(task__id=int(task_id), employee__id=int(employee_id)).count()
    response = {
        'disjunct_count' : count
    }
    return HttpResponse(json.dumps(response), content_type='application/json')


def list_doing_tasks(request, channel_id):
    project = Project.objects.get(id=int(channel_id))
    queryset  = Task.objects.filter(stage__stage_name=DOING_STAGE_NAME, project=project)
    task_serializer = TaskSerializer(queryset, many=True)
    return JsonResponse(task_serializer.data, status=201, safe=False)


def list_done_tasks(request, channel_id):
    project = Project.objects.get(id=int(channel_id))
    queryset  = Task.objects.filter(stage__stage_name=DONE_STAGE_NAME, project=project)
    task_serializer = TaskSerializer(queryset, many=True)
    return JsonResponse(task_serializer.data, status=201, safe=False)


# handle oauth flow
def handle_oauth_flow(request):
    auth_code = request.GET['code']
    exchange_url = 'https://slack.com/api/oauth.access?client_id={0}&client_secret={1}&code={2}'.format(CLIENT_ID, CLIENT_SECRET, auth_code)
    oauth_exchange_request = requests.get(exchange_url)
    the_dict = oauth_exchange_request.json()
    print(the_dict)
    bot_dict = the_dict.get('bot', None)
    team_name = the_dict.get('team_name', None)
    team_id = the_dict.get('team_id', None)
    if bot_dict is not None:
        bot_access_token = bot_dict.get('bot_access_token', None)
        if not Team.objects.filter(team_id=team_id).exists():
            notify_new_team.delay(team_id, bot_access_token)
            team = Team(access_token=bot_access_token, team_name=team_name, team_id=team_id)
            team.save() 
        return HttpResponseRedirect('http://www.phantomk8.com')
    else:
        the_signed_in_user_is_manager = False
        user_id = the_dict['user_id']
        access_token  = the_dict['access_token']
        #response = requests.get('https://slack.com/api/users.info?token={0}&user={1}&pretty=1'.format(access_token, user_id))
        team = Team.objects.get(team_id=team_id)
        users_detail = requests.get('https://slack.com/api/users.list?token={0}'.format(access_token))
        channels_detail = requests.get('https://slack.com/api/channels.list?token={0}'.format(access_token))
        users_detail = byteify(users_detail.json())
        channels_detail = byteify(channels_detail.json())
        member_list = users_detail['members']
        for member in member_list:
            print(member)
            unique_id = member['id']
            print(user_id)
            print(unique_id)
            username = member['name']
            is_bot = member['is_bot']
            is_deleted = member['deleted']
            if is_bot or unique_id == 'USLACKBOT' or is_deleted:
                continue
            email = member['profile']['email']
            is_admin = member['is_admin']
            if not Employee.objects.filter(unique_id=unique_id).exists():
                user = User.objects.create_user(username=unique_id, email=email, first_name=username)
                employee = Employee(user=user, unique_id=unique_id)
                employee.save()
                employee.team.add(team)
            else:
                employee = Employee.objects.get(unique_id=user_id)
                print(employee)
                print(employee.team.all())
                if team not in employee.team.all():
                    employee.team.add(team)
            if is_admin and user_id == unique_id:
                print("Im here")
                the_signed_in_user_is_manager = True
                
            
            if is_admin:
                manager, created = Manager.objects.get_or_create(employee_instance=employee)
        
        channel_list = channels_detail['channels']
        team_employees = team.employee_set.all()
        employee_mapping = {}
        for employee in team_employees:
            employee_mapping[employee.id] = employee
        
        for channel in channel_list:
            channel_id = channel['id']
            name = channel['name']
            is_general = channel['is_general']
            if name == 'random' or is_general:
                continue
            else:
                project, created = Project.objects.get_or_create(slack_channel_id=channel_id, team=team, defaults={'project_name': name, 'manager': manager})
                if created:
                    headers = {'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer b55df5347afe4002a39e94cd61c121c9'}
                    request_dict = {"value" : name, "synonyms" : [name]}
                    request_dict = json.dumps(request_dict)
                    entity_request = requests.post(PROJECT_ADDITION_URL, data=request_dict, headers=headers)
                    print(entity_request.json())
                ids = channel['members']
                for emp_id in ids:
                    if emp_id in employee_mapping.keys():
                        corresponding_employee = employee_mapping[emp_id]
                        project.employees.add(corresponding_employee)
        
        if the_signed_in_user_is_manager:
            return render(request, 'PGE/index.html', {'team_id' : team_id, 'username' : employee.user.first_name, 'manager_id': manager.id})
        else:
            return HttpResponse(status=403)
    
    return HttpResponse(status=200)


@csrf_exempt
def assign_tasks_gui(request, team_id):
    recieved_json = json.loads(request.body)
    print(recieved_json)
    recieved_dict = byteify(recieved_json)

    project_id = recieved_dict['project_id']
    task_name = recieved_dict['task_name']
    deadline = parser.parse(recieved_dict['deadline'])
    employees_id_list = recieved_dict['employee_id_list']

    project = Project.objects.get(id=int(project_id))
    stage, created = Stage.objects.get_or_create(stage_name=TODO_STAGE_NAME)
    stage.save()
    task = Task(task_name=task_name, project=project, deadline=deadline, stage=stage)
    task.save()
    channel_list = []
    task_team = []
    for emp_id in employees_id_list:
        employee = Employee.objects.get(id=int(emp_id))
        task_team.append(employee.user.first_name)
        channel_list.append(employee.slack_bot_channel_id)
        task.employees.add(employee)
    
    # Add task to agent

    headers = {'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer b55df5347afe4002a39e94cd61c121c9'}
    request_dict = {"value" : task.task_name, "synonyms" : [task.task_name]}
    request_dict = json.dumps(request_dict)
    entity_request = requests.post(TASK_ENTITY_ADDITION_URL, data=request_dict, headers=headers)
    print(entity_request.json())
    

    message_to_agent = "create disjuncts for {0}".format(task.task_name)
    print(message_to_agent)

    # add context(the newly created task) to all employees assigned to the task
    context_list = []
    context_dict = {"name": "considered_task", "lifespan": 5, "parameters": {"task" : task.task_name} }
    context_list.append(context_dict)
    headers = {'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer b55df5347afe4002a39e94cd61c121c9'}
    request_dict= json.dumps(context_list)
    for employee in task.employees.all():
        context_request = requests.post(CONTEXTS_ENDPOINT.format(employee.unique_id), data=request_dict, headers=headers)
        print(context_request.json())

    
    #send_automated_message_to_nlp(message_to_agent, task.employees.all())
    
    base_message = "Hey, you are assigned with a new task"
    task_description = "TASK: {0}, DEADLINE: {1} ".format(task.task_name, str(task.deadline.strftime('%Y-%m-%d')))
    project_detail = "PROJECT: {0}".format(project.project_name)
    task_team_string = "The task team: "
    create_disjunct_prompt = "Please provide your disjuncts for this task as a set of comma separated values. Example: disjunct_1, disjunct_2, disjunct_3."
    count = len(task_team)
    for index, name in enumerate(task_team):
        if index == count - 1:
            task_team_string = task_team_string + name + "."
        else:
            task_team_string = task_team_string + name + ", "


    message_list = [base_message, task_description, project_detail, task_team_string]
    the_message_string = '\n'.join(message_list)
    the_message_string = the_message_string + '\n' + create_disjunct_prompt
    
    notify_employees.delay(the_message_string, channel_list, team_id)
    
    response = {
        'task_id' : task.id
    }
    return HttpResponse(json.dumps(response), content_type="application/json") 


def handle_gui_signin(request):
    oauth_initiation_request = requests.get(OAUTH_SLACK_INITIATION_URL)


def developer_stage_instantiation(request):
    stage_doing = Stage(id=2, stage_name=DOING_STAGE_NAME)
    stage_doing.save()
    stage_done = Stage(id=3, stage_name=DONE_STAGE_NAME)
    stage_done.save()
    return HttpResponse(Stage.objects.all())

def handle_manager_view(request, team_id, channel_id):
    project = Project.objects.get(id=int(channel_id))
    queryset = project.task_set.all()
    task_serializer = TaskSerializer(queryset, many=True)
    return JsonResponse(task_serializer.data, status=201, safe=False)


def get_access_tokens(request):
    queryset = Team.objects.all()
    team_serializer = TeamSerializer(queryset, many=True)
    return JsonResponse(team_serializer.data, status=201, safe=False)


@csrf_exempt
def render_home(request):
    return render(request, 'PGE/phantomHome.html')
